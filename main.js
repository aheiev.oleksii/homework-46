const API = 'https://api.github.com';
const form = document.querySelector('form');
const result = document.querySelector('#result');
const avatar = document.querySelector('#avatar');
const repo = document.querySelector('#repo');
const followers = document.querySelector('#followers');
const following = document.querySelector('#following');

// async function controller(userName) {
//     try {
//         const response = await fetch(userName);
//         if (!response.ok) {
//             console.log('User not found');
//             return null;
//         }
//         const data = await response.json();
//         return data;
//     } catch (err) {
//         console.error(err.message);
//         return null;
//     }
// }

// form.addEventListener('submit', async e => {
//     e.preventDefault();
//     const userNmae = document.querySelector('#username').value;
//     const data = await controller(`${API}/users/${userNmae}`);
//     renderUserInfo(data);
// });

// function renderUserInfo(data) {
//     if (!data) {
//         result.style.display = 'none';
//         return;
//     }

//     result.style.display = 'block';
//     avatar.src = data.avatar_url;
//     repo.innerHTML = data.public_repos;
//     followers.innerHTML = data.followers;
//     following.innerHTML = data.following;
// }

// another way

form.addEventListener('submit', async (e) => {
    e.preventDefault();
    const userName = document.querySelector('#username').value;

    try {
        const response = await fetch(`${API}/users/${userName}`);
        result.style.display = 'block';

        if (!response.ok) {
            console.log('User not found');
            result.style.display = 'none';
        }

        const data = await response.json();

        avatar.src = data.avatar_url;
        repo.innerHTML = data.public_repos;
        followers.innerHTML = data.followers;
        following.innerHTML = data.following;
    } catch (err) {
        console.log(err.message);
    }
});